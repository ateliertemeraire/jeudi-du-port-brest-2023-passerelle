# Dessin à la scie cloche

Découpe à la scie cloche dans du médium en bois pour faire des pochoirs à partir des lignes d'une sélection de signe graphiques.

## Protocole

1. Extraction des lignes principales d'une série de signes graphiques (Logo, Picto, etc --*à préciser*-). Il ne s'agit pas de reprendre le contour de la forme mais plutôt les lignes directionnelles.
  
2. Création d'une série de patrons à imprimer, indiquant les points de départ et les directions de chaque tracé par un système de flèches. Le but étant de rendre rapidement reproductible le signe graphique au crayon sur le médium.
  
3. Reproduction du dessin sur médium.
  
4. Interprétation du dessin par la découpe à la scie cloche pour créer un pochoir.
  
5. Utilisation des pochoirs en peignant l'intérieur au pinceau.

![Notice](./img/Notice.jpg)