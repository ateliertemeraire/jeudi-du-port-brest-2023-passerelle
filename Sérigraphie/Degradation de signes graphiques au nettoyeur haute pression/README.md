# Dégradation de signes graphiques au nettoyeur haute-pression

Utilisation du nettoyeur haute-pression pour dégrader le pochoir de sérigraphie afin d'amplifier la résistance cognitive du signe.

## Protocole

1. Sélection de signes graphiques (Logo, Picto, etc --*à préciser*-).
2. Création d'une composition graphique pour un format de sérigraphie à partir de ces signes graphiques.
3. Insoler un cadre de sérigraphie avec le visuel et au moment du passage au nettoyeur haute-pression insister sur certaines zones pour détourner le design originel.
4. Impression

*Simulation numérique de l'effet*
![Simulation](./img/Simulation.jpg)